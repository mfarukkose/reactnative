

// import React, { Component } from 'react';
// import { GiftedChat } from 'react-native-gifted-chat'
// import io from 'socket.io-client/dist/socket.io'

// class App extends Component {

//   state = {
//     messages: [],
//     user: {
//       _id: "",
//     }
//   }

//   constructor(props) {
//     super(props);
//     this.socket = io("http://localhost:3001", {});
//     this.socket.on((msg) => {
//       console.log(msg);
//     });
//     this.socket.on("clientId", (clientID) => {
//       console.log(clientID);
//       // Alert.alert(clientID);
//       this.setState({
//         user: {
//           _id: clientID
//         }
//       })
//     })

//     this.socket.emit("ready");

//     this.socket.on("listen message", (messages) => {
//       this.setState(previousState => ({
//         messages: previousState.messages.some(e => e._id === messages._id)
//           ? previousState.messages
//           : GiftedChat.append(previousState.messages, messages),
//       }))

//     })
//   }

//   onSend(messages = []) {
//     this.socket.emit("send message", messages);
//     this.setState(previousState => ({
//       messages: GiftedChat.append(previousState.messages, messages),
//     }))
//   }

//   render() {
//     return (
//       <GiftedChat
//         messages={this.state.messages}
//         onSend={messages => this.onSend(messages)}
//         user={this.state.user}
//       />
//     );
//   }
// }

// // const App = () => (
// //   <Router>
// //     <Stack key="root">
// //       <Scene key="login" component={Login} title="Login" />
// //       <Scene key="Chat" component={Chat} title="Chat" />
// //     </Stack>
// //   </Router>
// // );

// export default App;


import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from './signIn';
import Chat from './chat';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ActivityIndicator } from "react-native";
const Stack = createStackNavigator();


const App = () => {

  const [isLoading, setIsLoading] = React.useState(true);
  const [user, setUser] = React.useState(true);

  React.useEffect(() => {

    AsyncStorage.getItem('currentUser').then(user => {
      if (user) {
        setUser(JSON.parse(user));
      }
    })


  });


  return (
    <NavigationContainer>
      {
        !isLoading ?
          <ActivityIndicator size="large" /> :
          <Stack.Navigator>
            {user ?
              <Stack.Screen name="Chat" component={Chat} /> :
              <Stack.Screen name="SignIn" component={SignIn} />
            }
          </Stack.Navigator>
      }

    </NavigationContainer>
  );
};

export default App;