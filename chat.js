import React, { Component } from 'react';
import { GiftedChat } from 'react-native-gifted-chat'
import io from 'socket.io-client/dist/socket.io'

class Chat extends Component {

    state = {
        messages: [],
        user: {
            _id: "",
        }
    }

    constructor(props) {
        super(props);
        this.socket = io("http://localhost:3001", {});
        this.socket.on((msg) => {
            console.log(msg);
        });
        this.socket.on("clientId", (clientID) => {
            console.log(clientID);
            // Alert.alert(clientID);
            this.setState({
                user: {
                    _id: clientID
                }
            })
        })

        this.socket.emit("ready");

        this.socket.on("listen message", (messages) => {
            this.setState(previousState => ({
                messages: previousState.messages.some(e => e._id === messages._id)
                    ? previousState.messages
                    : GiftedChat.append(previousState.messages, messages),
            }))

        })
    }

    onSend(messages = []) {
        this.socket.emit("send message", messages);
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }

    render() {
        return (
            <GiftedChat
                messages={this.state.messages}
                onSend={messages => this.onSend(messages)}
                user={this.state.user}
            />
        );
    }
}

export default Chat;