import React, { Component } from 'react';
import {
    ImageBackground,
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    Dimensions,
} from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import bgImages from './assets/images/background.jpg';
import AsyncStorage from '@react-native-async-storage/async-storage';

const { width: WIDTH } = Dimensions.get("window");
export default class SignIn extends Component {

    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            props: props,
        }

    }

    async signIn() {
        try {
            let response = await fetch('http://localhost:3001/signIn', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: this.state.username,
                    password: this.state.password,
                })
            });
            let json = await response.json();
            if (json.result._id) {
                const jsonValue = JSON.stringify(json.result)
                await AsyncStorage.setItem('currentUser', jsonValue)
                this.state.props.navigation.navigate('Chat', json.result);
            }
            console.log(json);
            // return json.movies;
        } catch (error) {
            console.error(error);
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground
                    blurRadius={10}
                    style={styles.backgroundContainer}
                    imageStyle={styles.imageStyle}
                    source={bgImages}>
                    <View>
                        <Text style={styles.loginText}>Sign In</Text>
                    </View>

                    <View>
                        <TextInput
                            placeholder={'Email'}
                            placeholderTextColor="#969696"
                            underlineColorAndroid="transparent"
                            style={styles.input}
                            onChangeText={(text) => this.setState({ username: text })}
                        >
                        </TextInput>
                    </View>
                    <View>
                        <TextInput
                            placeholder={'Password'}
                            placeholderTextColor="#969696"
                            underlineColorAndroid="transparent"
                            style={styles.input}
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({ password: text })}
                        >
                        </TextInput>

                    </View>
                    <View>
                        <TouchableOpacity style={styles.signinButton}
                            onPress={this.signIn.bind(this)}
                        >
                            <Text style={styles.signInText}>
                                SignIn
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
    },
    backgroundContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 0,
        resizeMode: 'stretch', // or 'stretch'
    },
    loginText: {
        color: 'white',
        fontSize: 35,
        fontWeight: "bold",
        marginBottom: 50,
    },
    input: {
        width: WIDTH - 55,
        height: 45,
        borderRadius: 29,
        fontSize: 20,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.9)',
        marginHorizontal: 30,
        paddingLeft: 30,
        marginVertical: 10,
    },
    inputIcon: {
        position: 'absolute',
        top: 10,
        left: 37,
    },
    signinButton: {
        width: WIDTH - 55,
        height: 45,
        borderRadius: 29,
        backgroundColor: '#432577',
        justifyContent: "center",
        marginTop: 20,

    },
    signInText: {
        color: 'rgba(255,255,255,0.9)',
        fontSize: 20,
        textAlign: "center",
    },
    imageStyle: {
        resizeMode: 'stretch'
    }
});
